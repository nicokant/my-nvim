" turn terminal to normal mode with escape
"tnoremap <Esc> <C-\><C-n>:q<CR>

" open terminal on ctrl+n
function! OpenTerminal()
  split term://bash
  resize 10
endfunction

"Mapping to open terminal emulator in nvim
nnoremap <c-t> :call OpenTerminal()<CR>

" " Put the following lines in your configuration file to map <F12> to use Multiterm
" nmap <c-t> <Plug>(Multiterm)
" " In terminal mode `count` is impossible to press, but you can still use <F12>
" " to close the current floating terminal window without specifying its tag
" tmap <c-t> <Plug>(Multiterm)
" " If you want to toggle in insert mode and visual mode
" imap <c-t> <Plug>(Multiterm)
" xmap <c-t> <Plug>(Multiterm)
