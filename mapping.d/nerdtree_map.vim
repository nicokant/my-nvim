"Toggle NERDTreeToggle on and of
nnoremap <c-e> :NERDTreeToggle<CR>

"directly open NerdTree on the file you’re editing to
"quickly perform operations on it with NERDTreeFind
nnoremap <silent> <Leader>v :NERDTreeFind<CR>
