"The system wide config file is found in /usr/share/nvim/sysinit.vim
"===================================================================
"===                 EDITOR CONFIGURATIONS                       ===
"===================================================================
source $XDG_CONFIG_HOME/nvim/editor.vim

"===================================================================
"===                TERMINAL CONFIGURATIONS                      ===
"===================================================================
source $XDG_CONFIG_HOME/nvim/terminal.vim

"===================================================================
"===                MAPPINGS ONFIGURATIONS                       ===
"===================================================================
source $XDG_CONFIG_HOME/nvim/mapinit.vim

"===================================================================
"===                PLUGINS CONFIGURATIONS                       ===
"===================================================================
source $XDG_CONFIG_HOME/nvim/pluginit.vim

"===================================================================
"===                UI CONFIGURATIONS                            ===
"===================================================================
source $XDG_CONFIG_HOME/nvim/ui.vim





" set encoding=UTF-8
" syntax on
" set number

" "set cursorline
" "set cursorcolumn

" " use current file directory as a start to find file to edit
" """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" map ,e :e <C-R>=expand("%:p:h") . "/" <CR>
" map ,t :tabe <C-R>=expand("%:p:h") . "/" <CR>
" map ,s :split <C-R>=expand("%:p:h") . "/" <CR>


" set wrap


" source ~/.config/nvim/plugins.vim
" source ~/.config/nvim/colors/theme.vim

" " open new split panes to right and below
" set splitright
" set splitbelow
